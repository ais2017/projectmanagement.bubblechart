/**
 * [logOut ]
 * @return {[type]} [description]
 */
function logOut() {
  // $.cookie('rcAuth', null);
  $.cookie("uname", null);
  $.cookie("realname", null);
  $.cookie("accToken", null);
  $.cookie("roleCookie", null);
  $(location).attr("href", "index.html");
};