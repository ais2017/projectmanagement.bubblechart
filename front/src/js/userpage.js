var serverPath = "http://127.0.0.1:8080";
// var serverPath = "http://185.189.13.93:8080";
var chart = null;
var globalId = 1;

var diagram = function (data) {
  let self = this;
  self.id = ko.observable(data.id);
  self.name = ko.observable(data.name);
  self.active = ko.observable(false);
  self.isPublic = ko.observable(true);
};


var project = function (data, diagramId) {
  let self = this;
   console.log("project constructor1");
  self.prid = ko.observable(data.id);
  self.name = ko.observable(data.name);
  self.drmid = ko.observable(diagramId);
   self.unicID = ko.computed(function() {
        return self.drmid() + "_" + self.prid();
    });
  self.active = ko.observable(false);
};
var copyproject = function (data) {
  console.log("project constructor2");
  let self = this;
  self.prid = ko.observable(data.prid());
  self.name = ko.observable(data.name());
  self.drmid = ko.observable(data.drmid());
  self.unicID = ko.computed(function() {
        return self.drmid() + " " + self.prid();
    });
  self.active = ko.observable(false);
};
// "x":{"name":"Цена","value":1.1},
// "y":{"name":"Площадь","value":2.3},
var atribute = function (data){ 
  let self = this;
  self.name =  data.name;
  self.value = data.value || 0;
};
var bubbleData = function (data,projectId,projectName){
  let self = this;
  self.projectId = projectId;
  self.projectName = projectName;
  self.x = data.x.value;
  self.y = data.y.value;
  self.t = data.t.value || "нет данных";
  self.r = data.r.value || 0;
  self.color = data.color;

};

var bubble = function (data, maxT){
  let self = this;
  self.id = ko.observable("bubbl_" + globalId);
  self.contid = ko.observable("cont_" + globalId);
  self.projectId = ko.observable(data.projectId);
  // self.projectName = ko.observable(data.projectName);
    self.projectName =  data.projectName;
  globalId ++;
  self.x = data.x;
  self.y = data.y;
  self.t = data.t || "нет данных";
  self.r = data.r || 0;
  self.color = getColor(data.t.value, maxT);
  self.active = ko.observable(false);
  // self.t.subscribe(function(item){
  //   console.log(item);
  //   self.color = getColor(item.value, maxT);
  // })
};

var diagramFull = function (data) {
	console.log("in data: ",data);
  let self = this;
  self.id = ko.observable(data.id);
  self.name = ko.observable(data.name);

	var maxT = 0;
	$.each(data.bubbles, function(){
			if(this.t.value > maxT) {
				maxT = this.t.value;
			};
		});
	console.log("maxT: ", maxT);

  self.bubbles = ko.computed(function(){
    var all = ko.observableArray([]);
    $.each(data.bubbles, function () {
      all.push(new bubble(this, maxT));
    });
    return all();
  });
self.projectsInDiagram = ko.computed(function(){
    var all = ko.observableArray([]);
    $.each(data.projectsInDiagram, function () {
      all.push(new project(this, data.id));
    });
    return all();
  });  
self.projectsNotInDiagram = ko.computed(function(){
    var all = ko.observableArray([]);
    $.each(data.projectsNotInDiagram, function () {
      all.push(new project(this, data.id));
    });
    return all();
  });
  console.log(self);
};

//----------- View Model --------------------------
function ViewModel() {
  console.group("init view model");
  let self = this;
  	self.diagrams = ko.observableArray([]),
    self.selectedDiagramId = ko.observable(null),
    self.selectedDiagram = ko.observable(null),
    self.chartData = ko.observableArray([]),
     self.selectedTask = ko.observable(),
    console.log("Model:", self);

     self.isTaskSelected = function(task) {
               return task == self.selectedTask();
            },

       self.clearTask = function(data, event) {
        console.log("Clear task:", data )
                if (data === self.selectedTask()) {
                    self.selectedTask(null);
                }
                if (data.x.value() === "") {
                   data.x.value() = 0;
                }
            },       
    ko.bindingHandlers.visibleAndSelect = {
            update: function(element, valueAccessor) {
                ko.bindingHandlers.visible.update(element, valueAccessor);
                if (valueAccessor()) {
                    setTimeout(function() {
                        $(element).find("input").focus().select();
                    }, 0); //new tasks are not in DOM yet
                }
            }
          }
  console.groupEnd();
};
/**
 * [Model description]
 * @type {ViewModel}
 */
var Model = new ViewModel();
ko.applyBindings(Model);

//----------- Subscribes --------------------------
Model.selectedDiagramId.subscribe(function (item) {
  Model.selectedDiagram(null);
  console.log(item);
  // console.log("selectedObjectId: this :", Model.selectedObject());
  if (item) {
    getDiagram();
  } else {
  }
});
Model.chartData.subscribe(function (item) {
   console.log("chartData subscribe");
   chart.invalidateData();
});

Model.selectedDiagram.subscribe(function (item) {
  console.groupCollapsed("Subscribe on selectedObject");
  if(!item){
    $("#chartDiv").html("");
    compileChart();
  } else {
    console.log(" Model.chartData():", Model.chartData());

    reCompileChartData();
    
    Model.chartData.push(new bubbleData({"color": "#967419",
"r": 1,
"t": 0,
"x": 0,
"y": 0}, 0, "noName"));
  	chart.data = Model.chartData();
    // chart.invalidateData();
  }
  console.groupEnd();
});

$(document).ready(function () {
  $("#accordion").accordion();
  console.group("document ready, initialisation: ");
   console.log( "uname", $.cookie("uname"));
  console.log( "realname", $.cookie("realname"));
   console.log( "accToken", $.cookie("accToken"));
   console.log( "roleCookie", $.cookie("roleCookie"));

     $("#add-diagram-form").form({
    fields: {
      name: {
        identifier: 'name',
        rules: [
          {
            type: 'empty',
            prompt: 'Введите имя'
          }
        ]
      },
      private_ch: {
        identifier: 'private_ch',
        rules: [
        ]
      }
    },
    onSuccess: function (event, fields) {
      event.preventDefault();
      console.group("createDiagram(name, public) ");
      createDiagram(fields.name, fields.private_ch); 
      console.groupEnd();
    }
  });

$('.ui.form.bullet')
  .form({
    fields: {
      x_field : ['decimal', 'empty'],
      y_field : ['decimal', 'empty'],
      r_field : ['decimal', 'empty'],
      t_field : ['decimal', 'empty']
    }
  });

  am4core.useTheme(am4themes_animated);

  getDiagramsList();

  compileChart();

  console.groupEnd();

});

function reCompileChartData(){
  if(Model.selectedDiagram()){
    Model.chartData.removeAll();
    // Model.chartData([]);
    $.each(Model.selectedDiagram().bubbles(), function(){
      console.log(this);
      // Model.chartData.removeAll();
      Model.chartData.push(new bubbleData(this, this.projectId(), this.projectName));
    });
    console.log("Model.chartData: ", Model.chartData());
  };
};

function getDiagramsList() {
  console.groupCollapsed("get Objects List");
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
      "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "GET",
    url: serverPath + "/bubble/diagrams",
    success: function (data, status, xhr) {
      console.info(data);
      Model.diagrams.removeAll();
		$.each(data.diagrams, function (index) {
          Model.diagrams.push(new diagram(this));
        });     	
    },
    error: function (data, status, xhr) {
      console.error(data);
      Model.diagrams.removeAll();
        try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке авторизации", "negative");
        };
    }
  });
  console.groupEnd();
};

function getDiagram() {
  console.groupCollapsed("get Objects List");
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
          "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "GET",
    url: serverPath + "/bubble/diagram?id=" + Model.selectedDiagramId(),
    success: function (data, status, xhr) {
      console.info(data);
      // Model.selectedDiagramId(null);
      Model.selectedDiagram(null);
      Model.selectedDiagram(new diagramFull(data));
     
    },
    error: function (data, status, xhr) {
      console.error(data);
      Model.selectedDiagram(null);
       try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке авторизации", "negative");
        };

    }
  });
  console.groupEnd();
};

function addProjectToDiagram(project) {
  console.groupCollapsed("add Project To Diagram");
  console.log(project);
  var dataToSend = new Object({
    "projectId": project.prid(),
    "diagramId": project.drmid()
  });
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
          "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "POST",
    data: JSON.stringify(dataToSend),
    url: serverPath + "/bubble/project",
    success: function (data, status, xhr) {
      console.info(data);
      getDiagram();
     
     
    },
    error: function (data, status, xhr) {
      console.error(data);
       try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке добавить проект", "negative");
        };      
    }
  });
  console.groupEnd();
};

// DELETE чтобы удалить проект  из диаграммы 
// http://localhost:8080/bubble/project?diagramId=1&projectId=1
function deleteProjectFromDiagram(project) {
  console.groupCollapsed("delete Project From Diagram");
 console.log(project);
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
          "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "DELETE",
    // data: JSON.stringify(dataToSend),
    url: serverPath + "/bubble/project?diagramId=" + project.drmid() + "&projectId=" + project.prid(),
    success: function (data, status, xhr) {
      console.info(data);
      getDiagram();
     
    },
    error: function (data, status, xhr) {
      console.error(data);
       try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке удалить проект", "negative");
        };      
    }
  });
  console.groupEnd();
};

// POST создать диаграмму 
// http://localhost:8080/bubble/diagram?id=1
function createDiagram(name, public) {
  console.groupCollapsed("add Project To Diagram");

  let tmppubl = true;
  if(!public){tmppubl = "true"};
  if(public == "on"){tmppubl = "false"}; 
  var dataToSend = new Object({
    "name": name,
    "isPublic2": tmppubl
  });
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
          "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "POST",
    data: JSON.stringify(dataToSend),
    url: serverPath + "/bubble/diagram",
    success: function (data, status, xhr) {
      console.info(data);
      getDiagramsList();
      // selectObjectOnClick(null, null, Model.selectedDiagramId());
    },
    error: function (data, status, xhr) {
      console.error(data);
       try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке авторизации", "negative");
        };      
    }
  });
  console.groupEnd();
};


function getColor(t, maxT) {
console.log("t: ",t);
var b = Math.floor(t/maxT * (10) + 15);
var r = Math.floor(t/maxT * (130) + 20);
var g = Math.floor(t/maxT * (10) + 106);
var newColor = '#' + rgb2hex(r, g, b);

return newColor;
};

function rgb2hex(r, g, b) {
return Number(r).toString(16).toUpperCase().replace(/^(.)$/, '0$1') +
Number(g).toString(16).toUpperCase().replace(/^(.)$/, '0$1') +
Number(b).toString(16).toUpperCase().replace(/^(.)$/, '0$1');
}

function findMaxT(){
	var maxT = 0;
     console.log("findMaxT selectedDiagram: ", Model.selectedDiagram());
	if(Model.selectedDiagram()){
    console.log("findMaxT in if ");
		$.each(Model.selectedDiagram().bubbles(), function(){
      console.log("findMaxT this: ", this);
			if(this.t.value > maxT) {
				maxT = this.t.value;
			}
		});
	};
	console.log("maxT: ",maxT);
	return maxT;
};

function selectObjectOnClick(el, ev, id) {
  console.group("select Diagram");
  console.info("id: ", id);
  if (!id) {
    console.log("not id");
    id = $(el).attr('id');
  }
  console.info("id from el: ", id);
  console.info("selectedDiagramId: ", Model.selectedDiagramId());
  if (Model.selectedDiagramId() != id) {
    if (Model.selectedDiagramId()) {
      var findObj = Model.diagrams().find(obj => obj.id() == Model.selectedDiagramId());
      console.log("findObj: ", findObj);
      findObj.active(false);
    }
    console.log("in if");
    Model.selectedDiagramId(id);
    findObj = Model.diagrams().find(obj => obj.id() == Model.selectedDiagramId());
    console.log("findObj: ", findObj);
    findObj.active(true);
  } else {
    console.log("in else");
    Model.diagrams().find(obj => obj.id() == Model.selectedDiagramId()).active(false);
    Model.selectedDiagramId(null);
  };
  console.groupEnd();
};

function clickOnBubbleItem(el, ev, id) {
  console.group("select Diagram");
  console.info("id: ", id);
  if (!id) {
    console.log("not id");
    id = $(el).attr('id');
  }
  var bubbl_id = id.split('_')[1];
  var cont_id = "cont_" + bubbl_id;
  if( $("#"+cont_id).hasClass("active")){
  $("#"+cont_id).removeClass("active");
  } else {
    $("#"+cont_id).addClass("active");
  }
 
  console.info("bubbl_id: ", bubbl_id);
  console.info("selectedDiagramId: ", Model.selectedDiagramId());
 
  console.groupEnd();
};
function changeBubbleItem(el, ev ) {
  console.group("select Diagram");
 
  var  id = $(el).parent().parent().attr("id");
    console.log(id);
  var bubbl_id = "bubbl_" + id.split('_')[1];
  console.log(bubbl_id);
  var tmpel = Model.selectedDiagram().bubbles().find(obj => obj.id() == bubbl_id);
  console.log(tmpel);
  tmpel.active(true);
   console.log(tmpel);
  console.info("bubbl_id: ", bubbl_id); 
 
  console.groupEnd();
};

function clearForm(el,ev){
  ev.preventDefault();
  // var arr = $(el).parent().find("input");
  // console.log(arr);
  // $.each(arr, function(){
  //   $(this).val("");
  // });
  // 
  $(el).parent().form('clear');
  var  id = $(el).parent().parent().parent().attr("id");
    console.log(id);
  var bubbl_id = "bubbl_" + id.split('_')[1];
  console.log(bubbl_id);
  var tmpel = Model.selectedDiagram().bubbles().find(obj => obj.id() == bubbl_id);
  console.log(tmpel);
  tmpel.active(false);

};

function sendNewAtributes(el,ev){
  ev.preventDefault();
  var  tmpform = $(el).parent();
  console.log(tmpform);
  console.log($(tmpform).form("validate form") );
  var fields = tmpform.form("get values", ['x_field', 'y_field', 'r_field','t_field'])
  console.log(fields);
  var res = true;
  if(!$.isNumeric( fields.x_field)) {
    res = false;
  }
    if(!$.isNumeric( fields.y_field)) {
    res = false;
  }
    if(!$.isNumeric( fields.t_field)) {
    res = false;
  }
    if(!$.isNumeric( fields.r_field)) {
    res = false;
  }
console.log(res);
  if(res){
      var  id = $(el).parent().parent().parent().attr("id");
      console.log(id);
      var bubbl_id = "bubbl_" + id.split('_')[1];
      console.log(bubbl_id);
      var tmpel = Model.selectedDiagram().bubbles().find(obj => obj.id() == bubbl_id); 
      tmpel.x.value = parseFloat(fields.x_field) ;  
      tmpel.y.value = parseFloat(fields.y_field) ; 
      tmpel.r.value = parseFloat(fields.r_field) ; 
      tmpel.t.value = parseFloat(fields.t_field) ; 
      console.log(tmpel);
      updateDiagram();
      
  } else {
    $(tmpform).form('clear');
  }
  // var arr = $(el).parent().find("input");
  // console.log(arr);
  // $.each(arr, function(){
  //   $(this).val("");
  // });
  // var  id = $(el).parent().parent().parent().attr("id");
  //   console.log(id);
  // var bubbl_id = "bubbl_" + id.split('_')[1];
  // console.log(bubbl_id);
  // var tmpel = Model.selectedDiagram().bubbles().find(obj => obj.id() == bubbl_id);
  // console.log(tmpel);
  // tmpel.active(false);

};


function updateDiagram(){
   console.groupCollapsed("update Diagram");
  var dataToSend =  ko.toJSON(Model.selectedDiagram());
  console.log(dataToSend);
  console.log(JSON.stringify(Model.selectedDiagram()));
  $.ajax({
    xhrFields: {
      // withCredentials: true
    },
    headers: {
          "Authorization": $.cookie("accToken")
    },
    contentType: "application/json",
    type: "PUT",
    data: dataToSend,
    url: serverPath + "/bubble/diagram?id=" + Model.selectedDiagramId(),
    success: function (data, status, xhr) {
      console.info(data);
      getDiagram();
       
    },
    error: function (data, status, xhr) {
      console.error(data);
       try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке авторизации", "negative");
        };      
    }
  });
  console.groupEnd();
};


function exchangeXY(){
  if(Model.selectedDiagram()){
    $.each(Model.selectedDiagram().bubbles(), function(){


      let tmpx = this.x;
      this.x = this.y;
      this.y = tmpx;
    });
    updateDiagram();
  }
};


function compileChart(){
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

chart = am4core.create("chartDiv", am4charts.XYChart);

var valueAxisX = chart.xAxes.push(new am4charts.ValueAxis());
valueAxisX.renderer.ticks.template.disabled = true;
valueAxisX.renderer.axisFills.template.disabled = true;

var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
valueAxisY.renderer.ticks.template.disabled = true;
valueAxisY.renderer.axisFills.template.disabled = true;

var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueX = "x";
series.dataFields.valueY = "y";
series.dataFields.value = "r";
series.strokeOpacity = 0;
series.sequencedInterpolation = true;
series.tooltip.pointerOrientation = "vertical";

var bullet = series.bullets.push(new am4charts.CircleBullet());
bullet.fill = am4core.color("#ff0000");
bullet.propertyFields.fill = "color";
bullet.strokeOpacity = 0;
bullet.strokeWidth = 2;
bullet.fillOpacity = 0.5;
bullet.stroke = am4core.color("#ffffff");
bullet.hiddenState.properties.opacity = 0;
bullet.circle.tooltipText = "[bold]Баббл соответствует проекту: {projectName}.[/]\nAtribute x: {valueX.value}\nAtribute y: {valueY.value}\nAtribute r: {value.value}\nAtribute t: {t}";
// bullet.circle.tooltipText = "[bold]{title}:[/]\nPopulation: {value.value}\nIncome: {valueX.value}\nLife expectancy:{valueY.value}";


bullet.events.on("over", function(event) {
    var target = event.target;
    chart.cursor.triggerMove({ x: target.pixelX, y: target.pixelY }, "hard");
    chart.cursor.lineX.y = target.pixelY;
    chart.cursor.lineY.x = target.pixelX - chart.plotContainer.pixelWidth;
    valueAxisX.tooltip.disabled = false;
    valueAxisY.tooltip.disabled = false;

    // outline.radius = target.circle.pixelRadius + 2;
    // outline.x = target.pixelX;
    // outline.y = target.pixelY;
    // outline.show();
})

bullet.events.on("out", function(event) {
    chart.cursor.triggerMove(event.pointer.point, "none");
    chart.cursor.lineX.y = 0;
    chart.cursor.lineY.x = 0;
    valueAxisX.tooltip.disabled = true;
    valueAxisY.tooltip.disabled = true;
    // outline.hide();
})

bullet.events.on("hit", function(event) {
    // chart.cursor.triggerMove(event.pointer.point, "none");
    // chart.cursor.lineX.y = 0;
    // chart.cursor.lineY.x = 0;
    // valueAxisX.tooltip.disabled = true;
    // valueAxisY.tooltip.disabled = true;
    // outline.hide();
   console.log("clicked on ", this);
})

var hoverState = bullet.states.create("hover");
hoverState.properties.fillOpacity = 1;
hoverState.properties.strokeOpacity = 1;

series.heatRules.push({ target: bullet.circle, min: 30, max:60, property: "radius" });

bullet.circle.adapter.add("tooltipY", function (tooltipY, target) {
    return -target.radius;
})

chart.cursor = new am4charts.XYCursor();
chart.cursor.behavior = "zoomXY";

chart.scrollbarX = new am4core.Scrollbar();
chart.scrollbarY = new am4core.Scrollbar();


// chart.data = Model.selectedDiagram().bubbles();
chart.data = [];

};

// 
// ------------------------------------------
// 

function delProject(el, ev) {
  console.group("del Project");
  console.info("this: ", el);
  
  let id = $(el).parent().parent().attr("id");

  console.info("id from el: ", id);

  let fProject = Model.selectedDiagram().projectsInDiagram().find(obj => obj.unicID() == id);
  // let fProject = Model.selectedDiagram().projectsInDiagram().findIndex(obj => obj.unicID() == id);
  // console.log(" finded project: ", fProject);
  // if(fProject !=-1){
  //   let el = Model.selectedDiagram().projectsInDiagram().splice(fProject, 1);
  //   Model.selectedDiagram().projectsNotInDiagram().push(el[0]);
  //   // Model.selectedDiagram().projectsInDiagram().remove(fProject);
  //   deleteProjectFromDiagram(el[0]);
  // }
  if(fProject !=-1){
    deleteProjectFromDiagram(fProject);
  }
  console.groupEnd();
};

function addProject(el, ev) {
  console.group("add Project");
  console.info("this: ", el);
  
  let id = $(el).parent().parent().attr("id");

  console.info("id from el: ", id);

  var fProject = Model.selectedDiagram().projectsNotInDiagram().find(obj => obj.unicID() == id);
  console.log("fProject", fProject)
  // let fProject = Model.selectedDiagram().projectsInDiagram().findIndex(obj => obj.unicID() == id);
  // console.log(" finded project: ", fProject);
  // if(fProject !=-1){
  //   let el = Model.selectedDiagram().projectsInDiagram().splice(fProject, 1);
  //   Model.selectedDiagram().projectsNotInDiagram().push(el[0]);
  //   // Model.selectedDiagram().projectsInDiagram().remove(fProject);
  //   deleteProjectFromDiagram(el[0]);
  // }
  if(fProject !=-1){
    addProjectToDiagram(fProject);
  }
  console.groupEnd();
};

//------------------- Alerts ----------------
function closeMessage(e){
  let target = $( e.target );
  console.log(target);
  target.closest('.message').closest('.item').remove();
 };

function pushAlert(message, type) {
  let newEl = $("<li class='item'><div class='ui large " + type +  " message'><i class='close icon' onclick='closeMessage(event)'></i><p>" + message + "</p></div></li>");
  $("#alertList").append(newEl);
};
/**
 * [logOut ]
 * @return {[type]} [description]
 */
function logOut() {
  // $.cookie('rcAuth', null);
  $.cookie("uname", null);
  $.cookie("realname", null);
  $.cookie("accToken", null);
  $.cookie("roleCookie", null);
  $(location).attr("href", "index.html");
};

//
//
//
var tmpdata = new Object(
{"id":1,"name":"first","bubbles":[{"x":3.0,"y":1.0,"r":2.0,"t":null},{"x":1.0,"y":2.0,"r":4.0,"t":2.0},{"x":2.0,"y":1.0,"r":2.0,"t":1.0}]}
    );

var tmplist = new Object({"diagrams":[{"id":1,"name":"first"},{"id":2,"name":"second"},{"id":3,"name":"третий"}]});
