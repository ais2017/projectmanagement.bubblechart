var serverPath = "http://127.0.0.1:8080";
// var serverPath = "http://185.189.13.93:8080";


if ($.cookie("accToken") && ($.cookie("accToken") != "null") && $.cookie("roleCookie") && $.cookie("roleCookie") != "null") {
  console.log($.cookie("accToken"));
  console.log($.cookie("roleCookie"));
  relocate($.cookie("roleCookie"));
};

$(document).ready(function () {
  console.group("document ready, initialisation: ")

  $("#login-form").form({
    fields: {
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: 'Введите логин (почту)'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Введите пароль'
          },
          {
            type   : 'minLength[1]',
            prompt : 'Ваш пароль должен состояить из {ruleValue} символов'
          }
        ]
      }
    },
    onSuccess: function (event, fields) {
      event.preventDefault();
      console.group("signUp");
   	  signUp(fields);
      console.groupEnd();
    }
  });

  console.groupEnd();

});

//----------------- check cookies function ----------
//----------------- check cookies ----------

//------------------ Sign up function --------------
//------------------ Sign up function --------------
function signUp(fields) {
	var dataToSend = new Object({
      userName: fields.email,
        password: fields.password		
	})
    $.ajax({
      xhrFields: {
        // withCredentials: true
      },
      headers: {
        // "Authorization": "Basic " + btoa(auth),
      },
      contentType: "application/json",
      method: "POST",
      url: serverPath + "/auth/token",
      // crossDomain: true,
      data: JSON.stringify(dataToSend),
      success: function (data, textStatus, xhr) {
        console.log(data); 
 		$.cookie("uname", data.userName);
        $.cookie("realname", data.realName);        
        $.cookie("accToken", data.token);
        $.cookie("roleCookie", data.role);
        relocate(data.role);
      },
      error: function (data) {
        console.log(data);
        try {
          pushAlert("Ошибка: " + data.status + " " + data.responseJSON.error_description, "negative");

        } catch (error) {
          pushAlert("Ошибка при попытке авторизации", "negative");
        };

      }
    });
};

//
//------------------- Alerts ----------------
function closeMessage(e){
	let target = $( e.target );
	console.log(target);
	target.closest('.message').closest('.item').remove();
 };

function pushAlert(message, type) {
  let newEl = $("<li class='item'><div class='ui large " + type +  " message'><i class='close icon' onclick='closeMessage(event)'></i><p>" + message + "</p></div></li>");
  $("#alertList").append(newEl);
};

function relocate(role) {
  if (role == "ADMIN" ) {
    $(location).attr("href", "adminka.html");
  } else if (role == "USER") {
    $(location).attr("href", "userpage.html");
  } else {
    pushAlert("Ошибка: нет такого пользователя", "negative");
  };
};
