package ru.mephi.bublechart.repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.mephi.bublechart.interfaces.dto.DiagramDto;
import ru.mephi.bublechart.model.*;
import ru.mephi.bublechart.web.dto.AttributeDto;
import ru.mephi.bublechart.web.dto.BubbleDto;
import ru.mephi.bublechart.web.dto.ProjectDto;
import ru.mephi.bublechart.web.dto.PutDiagramDto;

import javax.annotation.PostConstruct;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class BubbleDiagramRepositoryInMemoryImpl implements BubbleDiagramRepository {

    private int currentMaxId = 0;
    private List<BubbleDiagram> bubbleDiagrams = new ArrayList<>();
    private List<Project> projects = new ArrayList<>();
    BubbleDiagramRepositoryImpl bubbleDiagramRepository;
    JdbcTemplate jdbcTemplate1;

    @Autowired
    BubbleDiagramRepositoryInMemoryImpl(JdbcTemplate jdbcTemplate){
        jdbcTemplate1 = jdbcTemplate;
        this.bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(jdbcTemplate);
        bubbleDiagramRepository.init();
    }


    @PostConstruct
    void init() {
        // JdbcTemplate jdbcTemplate = null;
        // BubbleDiagramRepositoryImpl bubbleDiagramRepositoryImpl = new BubbleDiagramRepositoryImpl();
        // bubbleDiagramRepositoryImpl.init();
        // bubbleDiagramRepositoryImpl.findDiagramList(bubbleDiagramRepositoryImpl.getSelectDiagramsInfo());
        // System.out.println(bubbleDiagramRepositoryImpl.findDiagramList(bubbleDiagramRepositoryImpl.getSelectDiagramsInfo()));
        List<DiagramDto> diagramDtos = bubbleDiagramRepository.findDiagramList(bubbleDiagramRepository.getSelectDiagramsInfo());
        System.out.println(diagramDtos);
        //System.out.println(bubbleDiagramRepositoryImpl.findDiagramList("select id, name " + "FROM BubbleDiagram;"));
        BubbleDiagram b = new BubbleDiagram();
        b.setId(diagramDtos.get(0).getId());
        b.setNameOfBubbleDiagram(diagramDtos.get(0).getName());

        JdbcTemplate j = getJdbcTemplate1();
        System.out.println(j);
        //Project project = new Project();
//        String url = "jdbc:postgresql://localhost:5432/bubble";
//        String user = "bubble_user";
//        String password = "password";
//
//        try (Connection con = DriverManager.getConnection(url, user, password);
//             Statement st = con.createStatement();
//             ResultSet rs = st.executeQuery("select id, name FROM project")) {
//            if (rs.next()) {
//                //System.out.println(rs.getString(1));
//                project.setProjectid(rs.getInt("id"));
//                project.setProjectName(rs.getString("name"));
//            }
//        //            System.out.println(rs);
//        //            project.setProjectid(rs.getInt("id"));
//        //            project.setProjectName(rs.getString("name"));
//        }
//
//        catch (SQLException e) {
//            e.printStackTrace();
//        }

        Project project = new Project();
        project.setProjectName("First");
        project.setProjectid(1);
        //System.out.println(project.getProjectid());
        //System.out.println(project.getProjectName());
        //System.out.println(" ");



        //Project project = new Project();
        //project.setProjectName("First");
        //project.setProjectid(1);

        List<Factor> factors = new ArrayList<>();

        factors.add(new Factor("Цена", (float) 1.1));
        factors.add(new Factor("Площадь", (float) 2.3));
        factors.add(new Factor("Плотность населения", (float) 2.1));
        factors.add(new Factor("Прибыльность", (float) 10.2));

        project.setFactors(factors);

        projects.add(project);

        project = new Project();
        project.setProjectName("Second");
        project.setProjectid(2);

        factors = new ArrayList<>();

        factors.add(new Factor("Цена", (float) 1.5));
        factors.add(new Factor("Площадь", (float) 2.32));
        factors.add(new Factor("Плотность населения", (float) 4.1));
        factors.add(new Factor("Прибыльность", (float) 1.2));

        project.setFactors(factors);

        projects.add(project);

        int a = createNewDiagram(diagramDtos.get(0).getName(), "DEFAULT", true);
        addProjectToBubbleDiagram(a, projects.get(0).getProjectid());
    }

    @Override
    public List<DiagramDto> findDiagramList(String name) {
        List<DiagramDto> diagramDtos = new ArrayList<>();

        for (BubbleDiagram bubbleDiagram : bubbleDiagrams) {
            if (bubbleDiagram.isPublicDiagram() || bubbleDiagram.getUserName().equals(name)) {
                DiagramDto diagramDto = new DiagramDto();
                diagramDto.setId(bubbleDiagram.getId());
                diagramDto.setName(bubbleDiagram.getNameOfBubbleDiagram());

                diagramDtos.add(diagramDto);
            }
        }

        return diagramDtos;
    }

    @Override
    public List<ProjectDto> findProjectList() {
        List<ProjectDto> projectDtos = new ArrayList<>();

        for (Project project : projects) {
            ProjectDto projectDto = new ProjectDto();

            projectDto.setId(project.getProjectid());
            projectDto.setName(project.getProjectName());

            projectDtos.add(projectDto);
        }

        return projectDtos;
    }

    @Override
    public Integer deleteById(int id) {
        return null;
    }

    @Override
    public boolean deleteProjectFromDiagram(int diagramId, int projectId) {
        for (BubbleDiagram bubbleDiagram : bubbleDiagrams)
            if (bubbleDiagram.getId() == diagramId) {
                Project pr = null;
                for (Project project : bubbleDiagram.getProjects())
                    if (project.getProjectid() == projectId) {
                        pr = project;
                        break;
                    }

                Bubble b = null;
                for (Bubble bubble : bubbleDiagram.getBubbles())
                    if (bubble.getProject().getProjectid() == projectId) {
                        b = bubble;
                        break;
                    }

                if (pr == null || b == null) {
                    return false;
                }

                bubbleDiagram.getProjects().remove(pr);

                bubbleDiagram.getBubbles().remove(b);
            }
        return true;
    }

    @Override
    public BubbleDiagram findById(int id) {
        for (BubbleDiagram diagram : bubbleDiagrams) {
            if (diagram.getId() == id) {
                return diagram;
            }
        }
        return null;
    }

    @Override
    public Integer addBubbleDiagram(BubbleDiagram bubbleDiagram) {
        bubbleDiagrams.add(bubbleDiagram);
        return null;
    }

    @Override
    public Integer editBubbleDiagram(BubbleDiagram bubbleDiagram) {
        return null;
    }

    @Override
    public int createNewDiagram(String name, String userName, boolean isPuplic) {
        BubbleDiagram bubbleDiagram = new BubbleDiagram(name);
        bubbleDiagram.setPublicDiagram(isPuplic);
        bubbleDiagram.setId(currentMaxId + 1);
        currentMaxId++;
        bubbleDiagrams.add(bubbleDiagram);

        bubbleDiagram.setUserName(userName);
        return currentMaxId;
    }

    @Override
    public int createNewDiagram2(String name, String userName, String isPuplic) {
        BubbleDiagram bubbleDiagram = new BubbleDiagram(name);
        if (isPuplic.equals("true"))
            bubbleDiagram.setPublicDiagram(true);
        if (isPuplic.equals("false"))
            bubbleDiagram.setPublicDiagram(false);
        bubbleDiagram.setId(currentMaxId + 1);
        currentMaxId++;
        bubbleDiagrams.add(bubbleDiagram);

        bubbleDiagram.setUserName(userName);
        return currentMaxId;
    }

    @Override
    public boolean addProjectToBubbleDiagram(int bubbleDiagramId, int projectId) {

        Project project = null;

        for (Project proj : projects) {
            if (proj.getProjectid() == projectId) {
                project = proj;
            }
        }

        if (project == null) {
            return false;
        }

        for (BubbleDiagram bubbleDiagram : bubbleDiagrams) {
            if (bubbleDiagram.getId() == bubbleDiagramId) {
                bubbleDiagram.addProject(project);
                Bubble bubble = new Bubble();

                bubble.setProject(project);

                List<BubbleAttribute> attributes = new ArrayList<>();

                for (Factor factor : project.getFactors()) {
                    BubbleAttribute attribute = new BubbleAttribute();
                    attribute.setAttributeValue(factor.getFactorValue());
                    attribute.setAttributeName(factor.getFactorName());
                    attributes.add(attribute);
                }

                bubble.setBubbleAttributes(attributes);

                bubbleDiagram.getBubbles().add(bubble);
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean editDiagram(PutDiagramDto dto) {
        for (BubbleDiagram diagram : bubbleDiagrams) {
            if (diagram.getId() == dto.getId()) {

                List<BubbleDto> bubblesDto = dto.getBubbles();
                List<Bubble> bubbles = diagram.getBubbles();

                for (int i = 0; i < dto.getBubbles().size(); i++) {
                    BubbleDto bubbleDto = bubblesDto.get(i);
                    Bubble bubble = bubbles.get(i);

                    BubbleAttribute attribute = bubble.getBubbleAttributes().get(0);
                    AttributeDto attributeDto = bubbleDto.getX();
                    attribute.setAttributeName(attributeDto.getName());
                    attribute.setAttributeValue(attributeDto.getValue());

                    attribute = bubble.getBubbleAttributes().get(1);
                    attributeDto = bubbleDto.getY();
                    attribute.setAttributeName(attributeDto.getName());
                    attribute.setAttributeValue(attributeDto.getValue());


                    attribute = bubble.getBubbleAttributes().get(2);
                    attributeDto = bubbleDto.getR();
                    attribute.setAttributeName(attributeDto.getName());
                    attribute.setAttributeValue(attributeDto.getValue());


                    attribute = bubble.getBubbleAttributes().get(3);
                    attributeDto = bubbleDto.getT();
                    attribute.setAttributeName(attributeDto.getName());
                    attribute.setAttributeValue(attributeDto.getValue());


                }
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ProjectDto> findProjectsNotInDiagramById(int id) {

        List<ProjectDto> projectDtos = new ArrayList<>();

        for (BubbleDiagram diagram : bubbleDiagrams) {
            if (diagram.getId() == id) {
                for (Project project : projects) {
                    if (!diagram.getProjects().contains(project)) {
                        ProjectDto projectDto = new ProjectDto();
                        projectDto.setName(project.getProjectName());
                        projectDto.setId(project.getProjectid());
                        projectDtos.add(projectDto);
                    }
                }

            }
        }

        return projectDtos;
    }

    @Override
    public List<ProjectDto> findProjectsInDiagramById(int id) {
        List<ProjectDto> projectDtos = new ArrayList<>();
        for (BubbleDiagram diagram : bubbleDiagrams) {
            if (diagram.getId() == id) {
                for (Project project : diagram.getProjects()) {
                    ProjectDto projectDto = new ProjectDto();
                    projectDto.setName(project.getProjectName());
                    projectDto.setId(project.getProjectid());
                    projectDtos.add(projectDto);
                }
            }
        }
        return projectDtos;
    }
    //: достать все поля этого объяекта чтобы обратиться по нему потом в тестах!
    public JdbcTemplate getJdbcTemplate1() {
        return jdbcTemplate1;
    }

    public void initialize() {
        Project project = new Project();
        project.setProjectName("First");
        project.setProjectid(1);
        //System.out.println(project.getProjectid());
        //System.out.println(project.getProjectName());
        //System.out.println(" ");



        //Project project = new Project();
        //project.setProjectName("First");
        //project.setProjectid(1);

        List<Factor> factors = new ArrayList<>();

        factors.add(new Factor("Цена", (float) 1.1));
        factors.add(new Factor("Площадь", (float) 2.3));
        factors.add(new Factor("Плотность населения", (float) 2.1));
        factors.add(new Factor("Прибыльность", (float) 10.2));

        project.setFactors(factors);

        projects.add(project);

        project = new Project();
        project.setProjectName("Second");
        project.setProjectid(2);

        factors = new ArrayList<>();

        factors.add(new Factor("Цена", (float) 1.5));
        factors.add(new Factor("Площадь", (float) 2.32));
        factors.add(new Factor("Плотность населения", (float) 4.1));
        factors.add(new Factor("Прибыльность", (float) 1.2));

        project.setFactors(factors);

        projects.add(project);

        int a = createNewDiagram("firstDiagram", "DEFAULT", true);
        addProjectToBubbleDiagram(a, projects.get(0).getProjectid());
    }
}
