package ru.mephi.bublechart.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mephi.bublechart.users.User;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryImplTest {

    private UserRepositoryImpl userRepository;

    @BeforeEach
    void init() {
        userRepository = new UserRepositoryImpl();
    }

    @Test
    void findByName() {
        userRepository.init();
        assertEquals("a", userRepository.findByName("a").getName());
        assertNull(userRepository.findByName("unUsedName"));
    }

    @Test
    void addUser() {
        User user = new User();
        user.setName("name");
        assertEquals(user, userRepository.addUser(user));
        assertNull(userRepository.addUser(user));

    }

    @Test
    void editUser() {
        User user = new User();
        User user2 = new User();
        user.setName("name");
        user2.setName("name2");
        userRepository.addUser(user);
        assertNull(userRepository.editUser(user2));
        assertEquals(user, userRepository.editUser(user));
    }

    @Test
    void findAll() {
        User user = new User();
        User user2 = new User();
        user.setName("name");
        user2.setName("name2");
        userRepository.addUser(user);
        userRepository.addUser(user2);
        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);
        assertEquals(users, userRepository.findAll());
    }

    @Test
    void findRoleByToken() {
        User user = new User();
        User user2 = new User();
        user.setName("name");
        user.setToken("aaaaaaaaaa");
        user.setRole("admin");
        user2.setName("name2");
        user2.setToken(null);
        user2.setRole("user");
        userRepository.addUser(user);
        userRepository.addUser(user2);
        assertEquals("admin",userRepository.findRoleByToken("aaaaaaaaaa") );
        assertNull(userRepository.findRoleByToken("tokenName"));
        assertNull(userRepository.findRoleByToken(null));
    }

    @Test
    void findRoleNameByToken() {
        User user = new User();
        User user2 = new User();
        user.setName("name");
        user.setToken("aaaaaaaaaa");
        user.setRole("admin");
        user2.setName("name2");
        user2.setToken(null);
        user2.setRole("user");
        userRepository.addUser(user);
        userRepository.addUser(user2);
        assertEquals("name",userRepository.findRoleNameByToken("aaaaaaaaaa") );
        assertNull(userRepository.findRoleNameByToken("tokenName"));
        assertNull(userRepository.findRoleNameByToken(null));
    }
}