package ru.mephi.bublechart.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.postgresql.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import ru.mephi.bublechart.interfaces.BubbleDiagramService;
import ru.mephi.bublechart.interfaces.BubbleDiagramServiceImpl;
import ru.mephi.bublechart.interfaces.dto.DiagramDto;
import ru.mephi.bublechart.model.BubbleDiagram;
import ru.mephi.bublechart.model.Project;
import ru.mephi.bublechart.web.dto.AttributeDto;
import ru.mephi.bublechart.web.dto.BubbleDto;
import ru.mephi.bublechart.web.dto.PutDiagramDto;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

//@Component
@ExtendWith(MockitoExtension.class)
class BubbleDiagramRepositoryInMemoryImplTest {
    //@Autowired

    @Bean
    JdbcTemplate jdbcTemplate() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        // extract this 4 parameters using your own logic
        final String driverClassName = "org.postgresql.Driver";
        final String jdbcUrl = "jdbc:postgresql://localhost:5432/bubble";
        final String username = "bubble_user";
        final String password = "password";
        // Build dataSource manually:
        final Class<?> driverClass = ClassUtils.resolveClassName(driverClassName, this.getClass().getClassLoader());
        final Driver driver = (Driver) Objects.requireNonNull(ClassUtils.getConstructorIfAvailable(driverClass)).newInstance();
        final DataSource dataSource = new SimpleDriverDataSource(driver, jdbcUrl, username, password);
        // or using DataSourceBuilder:
        //final DataSource dataSource = DataSourceBuilder.create().driverClassName(driverClassName).url(jdbcUrl).username(username).password(password).build();
        // and make the jdbcTemplate
        return new JdbcTemplate(dataSource);
    }


    @Mock
    JdbcTemplate jdbcTemplate1;

    //private BubbleDiagramRepositoryInMemoryImpl bubbleDiagramRepository;

//    @Autowired
//    BubbleDiagramRepositoryInMemoryImplTest(JdbcTemplate jdbcTemplate){
//        this.bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(jdbcTemplate);
//        bubbleDiagramRepository.init();
//    }

    @Mock
    BubbleDiagramRepositoryImpl bubbleDiagramRepository;
    @Autowired
    //@MockBean
    BubbleDiagramRepositoryInMemoryImpl bubbleDiagramRepositoryInMemory;

//    @Autowired
//    BubbleDiagramRepositoryInMemoryImplTest(){
//        this.bubbleDiagramRepositoryInMemory = new BubbleDiagramRepositoryInMemoryImpl(jdbcTemplate);
//        //bubbleDiagramRepositoryInMemory.init();
//    }

//    @Autowired
//    BubbleDiagramRepositoryInMemoryImplTest(JdbcTemplate jdbcTemplate){
//        this.bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(jdbcTemplate);
//        bubbleDiagramRepository.init();
//    }
    private BubbleDiagramService bubbleDiagramService;

    private BubbleDiagram bubbleDiagram;

    @BeforeEach
    @Autowired
    void init() {
        //int a =0;
        this.bubbleDiagramRepositoryInMemory = new BubbleDiagramRepositoryInMemoryImpl(jdbcTemplate1);
        //this.bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(jdbcTemplate);
        //this.bubbleDiagramRepositoryInMemory.init();

       // bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(jdbcTemplate);
        //bubbleDiagramService = new BubbleDiagramServiceImpl(bubbleDiagramRepositoryInMemory);
        //bubbleDiagramService = new BubbleDiagramServiceImpl(bubbleDiagramRepository);
//        JdbcTemplate jdbcTemplate = null;
//        bubbleDiagramRepository = new BubbleDiagramRepositoryImpl(bubbleDiagramRepository);
//        bubbleDiagram = new BubbleDiagram("Diagram");

    }

    @Test
    void memTest() {
        bubbleDiagramRepositoryInMemory.findById(1);
    }

    @Test
    void initTest() throws IllegalAccessException, InstantiationException, InvocationTargetException {

        JdbcTemplate j = new JdbcTemplate();
        j = jdbcTemplate();
        //DataSource dataSource = new DataSource();
        //j.setDataSource();
        //Mockito.when(bubbleDiagramRepository.findDiagramList("name"))
        //        .thenReturn(Arrays.asList(new DiagramDto(), new DiagramDto()));
        BubbleDiagramRepositoryInMemoryImpl bubbleDiagramRepositoryInMemory1 = new BubbleDiagramRepositoryInMemoryImpl(j);

        bubbleDiagramRepositoryInMemory1.init();
    }

    @Test
    void findDiagramList() {
        //this.bubbleDiagramRepositoryInMemory = new BubbleDiagramRepositoryInMemoryImpl(jdbcTemplate);
        // Mockito.when(bubbleDiagramRepositoryInMemory.findDiagramList("name"))
        //        .thenReturn(Arrays.asList(new DiagramDto(), new DiagramDto()));
        BubbleDiagram bubbleDiagram1 = new BubbleDiagram();
        bubbleDiagram1.setUserName("name");
        bubbleDiagram1.setNameOfBubbleDiagram("name_DB");
        bubbleDiagram1.setId(1);
        bubbleDiagram1.setPublicDiagram(true);
        BubbleDiagram bubbleDiagram2 = new BubbleDiagram();
        bubbleDiagram2.setUserName("name");
        bubbleDiagram2.setNameOfBubbleDiagram("name_DB2");
        bubbleDiagram2.setId(2);
        bubbleDiagram2.setPublicDiagram(false);
        bubbleDiagramRepositoryInMemory.addBubbleDiagram(bubbleDiagram1);
        bubbleDiagramRepositoryInMemory.addBubbleDiagram(bubbleDiagram2);
        List<DiagramDto> diagramList = bubbleDiagramRepositoryInMemory.findDiagramList("name");
        //System.out.println(diagramList.size());
        assertEquals(2, diagramList.size());
        //bubbleDiagramRepositoryInMemory.findDiagramList("");
    }

    @Test
    void findProjectList() {
        //Project project = new Project();
        ////List<DiagramDto> diagramDtos = new ArrayList<>();
       /// DiagramDto diagramDto = new DiagramDto();
        //diagramDtos.add(diagramDto);
        ///Mockito.when(bubbleDiagramRepository.findDiagramList("select id, name " +
        ////        "FROM BubbleDiagram;")).thenReturn(diagramDtos);

        bubbleDiagramRepositoryInMemory.initialize();
        //bubbleDiagramRepositoryInMemory.addProjectToBubbleDiagram(1, 2);
        assertEquals(2, bubbleDiagramRepositoryInMemory.findProjectList().size());
    }

    @Test
    void deleteById() {
        assertNull(bubbleDiagramRepositoryInMemory.deleteById(1));
    }

    @Test
    void deleteProjectFromDiagram() {
        bubbleDiagramRepositoryInMemory.initialize();
        assertTrue(bubbleDiagramRepositoryInMemory.deleteProjectFromDiagram(1, 1));
        //assertTrue(bubbleDiagramRepositoryInMemory.deleteProjectFromDiagram(10, 1));
        assertFalse(bubbleDiagramRepositoryInMemory.deleteProjectFromDiagram(1, 1));
    }

    @Test
    void findById() {
        bubbleDiagramRepositoryInMemory.initialize();
        assertEquals(1, bubbleDiagramRepositoryInMemory.findById(1).getId());
        assertNull(bubbleDiagramRepositoryInMemory.findById(100));
    }

    @Test
    void addBubbleDiagram() {
        BubbleDiagram bubbleDiagram1 = new BubbleDiagram();
        bubbleDiagram1.setUserName("name");
        bubbleDiagram1.setNameOfBubbleDiagram("name_DB");
        bubbleDiagram1.setId(1);
        bubbleDiagram1.setPublicDiagram(true);
        bubbleDiagramRepositoryInMemory.addBubbleDiagram(bubbleDiagram1);
        List<DiagramDto> diagramList = bubbleDiagramRepositoryInMemory.findDiagramList("name");
        assertEquals(1, diagramList.size());
    }

    @Test
    void editBubbleDiagram() {
        BubbleDiagram bubbleDiagram1 = new BubbleDiagram();
        assertNull(bubbleDiagramRepositoryInMemory.editBubbleDiagram(bubbleDiagram1));
    }

    @Test
    void createNewDiagram() {
        assertEquals(1,bubbleDiagramRepositoryInMemory.createNewDiagram("name", "userName", true));
    }

    @Test
    void createNewDiagram2() {
        assertEquals(1,bubbleDiagramRepositoryInMemory.createNewDiagram2("name", "userName", "true"));
        assertEquals(2,bubbleDiagramRepositoryInMemory.createNewDiagram2("name", "userName", "false"));
    }

    @Test
    void addProjectToBubbleDiagram() {
        bubbleDiagramRepositoryInMemory.initialize();
        assertFalse(bubbleDiagramRepositoryInMemory.addProjectToBubbleDiagram(1,10));
        assertFalse(bubbleDiagramRepositoryInMemory.addProjectToBubbleDiagram(91,1));
        assertTrue(bubbleDiagramRepositoryInMemory.addProjectToBubbleDiagram(1,1));
    }

    @Test
    void editDiagram() {
        AttributeDto attributeDto1 = new AttributeDto();
        AttributeDto attributeDto2 = new AttributeDto();
        AttributeDto attributeDto3 = new AttributeDto();
        AttributeDto attributeDto4 = new AttributeDto();

        attributeDto1.setName("name1");
        attributeDto1.setValue(2.0f);

        attributeDto2.setName("name2");
        attributeDto2.setValue(1.0f);

        attributeDto3.setName("name3");
        attributeDto3.setValue(3.0f);

        attributeDto4.setName("name4");
        attributeDto4.setValue(4.0f);


        BubbleDto bubbleDto1 = new BubbleDto();
        BubbleDto bubbleDto2 = new BubbleDto();
        BubbleDto bubbleDto3 = new BubbleDto();
        BubbleDto bubbleDto4 = new BubbleDto();

        bubbleDto1.setX(attributeDto1);
        bubbleDto1.setY(attributeDto2);
        bubbleDto1.setR(attributeDto3);
        bubbleDto1.setT(attributeDto4);

        bubbleDto2.setX(attributeDto1);
        bubbleDto2.setY(attributeDto2);
        bubbleDto2.setR(attributeDto3);
        bubbleDto2.setT(attributeDto4);

        bubbleDto3.setX(attributeDto1);
        bubbleDto3.setY(attributeDto2);
        bubbleDto3.setR(attributeDto3);
        bubbleDto3.setT(attributeDto4);

        bubbleDto4.setX(attributeDto1);
        bubbleDto4.setY(attributeDto2);
        bubbleDto4.setR(attributeDto3);
        bubbleDto4.setT(attributeDto4);

        List<BubbleDto> bubbleDtos = new ArrayList<>();
        bubbleDtos.add(bubbleDto1);

        bubbleDiagramRepositoryInMemory.initialize();
        PutDiagramDto putDiagramDto = new PutDiagramDto();
        PutDiagramDto putDiagramDto1 = new PutDiagramDto();
        PutDiagramDto putDiagramDto2 = new PutDiagramDto();
        putDiagramDto.setId(1);
        putDiagramDto1.setId(9);
        putDiagramDto2.setId(1);
        putDiagramDto2.setBubbles(bubbleDtos);
        assertTrue(bubbleDiagramRepositoryInMemory.editDiagram(putDiagramDto));
        assertFalse(bubbleDiagramRepositoryInMemory.editDiagram(putDiagramDto1));
        assertTrue(bubbleDiagramRepositoryInMemory.editDiagram(putDiagramDto2));

    }

    @Test
    void findProjectsNotInDiagramById() {
        bubbleDiagramRepositoryInMemory.initialize();
        assertEquals(1, bubbleDiagramRepositoryInMemory.findProjectsNotInDiagramById(1).size());
        assertEquals(0, bubbleDiagramRepositoryInMemory.findProjectsNotInDiagramById(10).size());
    }

    @Test
    void findProjectsInDiagramById() {
        bubbleDiagramRepositoryInMemory.initialize();
        assertEquals(1, bubbleDiagramRepositoryInMemory.findProjectsInDiagramById(1).size());
        assertEquals(0, bubbleDiagramRepositoryInMemory.findProjectsInDiagramById(10).size());
    }
}