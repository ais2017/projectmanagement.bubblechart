package ru.mephi.bublechart.repository.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SelectBubbleDiagramByIdDtoTest {
    SelectBubbleDiagramByIdDto selectBubbleDiagramByIdDto;

    @BeforeEach
    void init() {
        selectBubbleDiagramByIdDto = new SelectBubbleDiagramByIdDto();
    }

    @Test
    void getId() {
        SelectBubbleDiagramByIdDto s1 = new SelectBubbleDiagramByIdDto(1,2,"name", 3);
        assertEquals((Integer) 1, s1.getId());
    }

    @Test
    void getBubbleId() {
        SelectBubbleDiagramByIdDto s1 = new SelectBubbleDiagramByIdDto(1,2,"name", 3);
        assertEquals((Integer) 2, s1.getBubbleId());
    }

    @Test
    void getName() {
        SelectBubbleDiagramByIdDto s1 = new SelectBubbleDiagramByIdDto(1,2,"name", 3);
        assertEquals("name", s1.getName());
    }

    @Test
    void getValue() {
        SelectBubbleDiagramByIdDto s1 = new SelectBubbleDiagramByIdDto(1,2,"name", 3);
        assertEquals((Integer) 3, s1.getValue());
    }

    @Test
    void setId() {
        selectBubbleDiagramByIdDto.setId(1);
        assertEquals((Integer) 1, selectBubbleDiagramByIdDto.getId());
    }

    @Test
    void setBubbleId() {
        selectBubbleDiagramByIdDto.setBubbleId(1);
        assertEquals((Integer) 1, selectBubbleDiagramByIdDto.getBubbleId());
    }

    @Test
    void setName() {
        selectBubbleDiagramByIdDto.setName("name");
        assertEquals( "name", selectBubbleDiagramByIdDto.getName());

    }

    @Test
    void setValue() {
        selectBubbleDiagramByIdDto.setValue(1);
        assertEquals((Integer) 1, selectBubbleDiagramByIdDto.getValue());
    }
}