package ru.mephi.bublechart.mapper;

import org.junit.jupiter.api.Test;
import ru.mephi.bublechart.model.Bubble;
import ru.mephi.bublechart.model.BubbleAttribute;
import ru.mephi.bublechart.model.BubbleDiagram;
import ru.mephi.bublechart.model.Type;
import ru.mephi.bublechart.web.dto.DiagramDto;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DiagramMapperTest {

    @Test
    void map() {
        DiagramDto dto = new DiagramDto();

        BubbleDiagram bubbleDiagram = new BubbleDiagram();
        bubbleDiagram.setId(1);
        bubbleDiagram.setPublicDiagram(true);
        bubbleDiagram.setNameOfBubbleDiagram("NameBD");
        List<Bubble> bubbles = new ArrayList<>();
        Bubble bubble_1 = new Bubble();
        List<BubbleAttribute> attributes_1 = new ArrayList<>();

        BubbleAttribute attribute_1 = new BubbleAttribute();
        attribute_1.setAttributeName("firstAttributeName");
        attribute_1.setAttributeValue(10);
        attributes_1.add(attribute_1);

        BubbleAttribute attribute_2 = new BubbleAttribute();
        attribute_2.setAttributeName("secondAttributeName");
        attribute_2.setAttributeValue(20);
        attributes_1.add(attribute_2);

        BubbleAttribute attribute_3 = new BubbleAttribute();
        attribute_3.setAttributeName("thirdAttributeName");
        attribute_3.setAttributeValue(30);
        attributes_1.add(attribute_3);

        BubbleAttribute attribute_4 = new BubbleAttribute();
        attribute_4.setAttributeName("fourthAttributeName");
        attribute_4.setAttributeValue(40);
        attributes_1.add(attribute_4);

        bubble_1.setBubbleAttributes(attributes_1);
        bubbles.add(bubble_1);

        Bubble bubble_2 = new Bubble();
        List<BubbleAttribute> attributes_2 = new ArrayList<>();

        BubbleAttribute attribute_5 = new BubbleAttribute();
        attribute_5.setAttributeName("fifthAttributeName");
        attribute_5.setAttributeValue(50);
        attributes_2.add(attribute_5);

        BubbleAttribute attribute_6 = new BubbleAttribute();
        attribute_6.setAttributeName("sixthAttributeName");
        attribute_6.setAttributeValue(60);
        attributes_2.add(attribute_6);

        bubble_2.setBubbleAttributes(attributes_2);
        bubbles.add(bubble_2);

        bubbleDiagram.setBubbles(bubbles);

        DiagramMapper diagramMapper = new DiagramMapper();
        dto = diagramMapper.map(bubbleDiagram);

        assertEquals((Integer) 1, dto.getId());
        assertEquals("NameBD", dto.getName());
        assertEquals((Float) 10.0f, dto.getBubbles().get(0).getX().getValue());
        assertEquals("firstAttributeName", dto.getBubbles().get(0).getX().getName());
        assertEquals((Float) 20.0f, dto.getBubbles().get(0).getY().getValue());
        assertEquals("secondAttributeName", dto.getBubbles().get(0).getY().getName());
        assertEquals((Float) 30.0f, dto.getBubbles().get(0).getR().getValue());
        assertEquals("thirdAttributeName", dto.getBubbles().get(0).getR().getName());
        assertEquals((Float) 40.0f, dto.getBubbles().get(0).getT().getValue());
        assertEquals("fourthAttributeName", dto.getBubbles().get(0).getT().getName());
        assertEquals((Float) 50.0f, dto.getBubbles().get(1).getX().getValue());
        assertEquals("fifthAttributeName", dto.getBubbles().get(1).getX().getName());
        assertEquals((Float) 60.0f, dto.getBubbles().get(1).getY().getValue());
        assertEquals("sixthAttributeName", dto.getBubbles().get(1).getY().getName());

    }
}