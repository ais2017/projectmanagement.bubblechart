package ru.mephi.bublechart.users;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    private User user;

    @BeforeEach
    void init(){
        user = new User();
        user.setId(1);
        user.setName("name");
        user.setPassword("password");
        user.setRealName("realName");
        user.setRole("user");
        user.setToken("eAEKK9c3vJGQuhuf");
    }

    @Test
    void getId() {
        assertEquals( (Integer) 1, user.getId());
    }

    @Test
    void setId() {
        user.setId(2);
        assertEquals( (Integer) 2, user.getId());
    }

    @Test
    void getName() {
        assertEquals("name", user.getName());
    }

    @Test
    void setName() {
        user.setName("newName");
        assertEquals("newName", user.getName());
    }

    @Test
    void getRole() {
        assertEquals("user", user.getRole());
    }

    @Test
    void setRole() {
        user.setRole("admin");
        assertEquals("admin", user.getRole());
    }

    @Test
    void getPassword() {
        assertEquals("password", user.getPassword());
    }

    @Test
    void setPassword() {
        user.setPassword("newPassword");
        assertEquals("newPassword", user.getPassword());
    }

    @Test
    void getToken() {
        assertEquals("eAEKK9c3vJGQuhuf", user.getToken());
    }

    @Test
    void setToken() {
        user.setToken("eRGDS3v5ewdDDFGG");
        assertEquals("eRGDS3v5ewdDDFGG", user.getToken());
    }

    @Test
    void getRealName() {
        assertEquals("realName", user.getRealName());
    }

    @Test
    void setRealName() {
        user.setRealName("Leonid");
        assertEquals("Leonid", user.getRealName());
    }
}